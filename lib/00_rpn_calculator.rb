class RPNCalculator
  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def plus
    ensure_stack_not_empty
    num1 = @stack.pop
    num2 = @stack.pop
    @stack << num1 + num2
  end

  def minus
    ensure_stack_not_empty
    num1 = @stack.pop
    num2 = @stack.pop
    @stack << num2 - num1
  end

  def times
    ensure_stack_not_empty
    num1 = @stack.pop
    num2 = @stack.pop
    @stack << num1 * num2
  end

  def divide
    ensure_stack_not_empty
    num1 = @stack.pop
    num2 = @stack.pop
    @stack << num2.to_f / num1
  end

  def value
    @stack.last
  end

  def tokens(str)
    str.split.map do |el|
      case el
      when "+"
        :+
      when "-"
        :-
      when "*"
        :*
      when "/"
        :/
      else
        el.to_i
      end
    end
  end

  def evaluate(str)
    tokens(str).each do |token|
      case token
      when token.class == Fixnum
        @stack.push(token)
      when :+
        @stack.plus
      when :-
        @stack.minus
      when :*
        @stack.times
      when :/
        @stack.divide
      end
    end
    @stack.value
  end

  private

  def ensure_stack_not_empty
    raise "calculator is empty" if @stack.length < 2
  end
end
